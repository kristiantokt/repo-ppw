from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from .views import index, friend_list, get_friend_list, add_friend, delete_friend, validate_npm, model_to_dict
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_paginate_exception_not_an_integer(self):
        response = Client().get('/lab-7/?page=this')
        self.assertEqual(response.status_code, 200)
    
    def test_paginate_exception_empty_data(self):
        response = Client().get('/lab-7/?page=11')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_is_done(self):
        response_post = Client().post('/lab-7/add-friend/', {'name': "Hore", 'npm': "1606000001", 'alamat':"Bekasi"})
        self.assertEqual(response_post.status_code, 200)

    def test_delete_friend_is_done(self):
        friend = Friend.objects.create(friend_name = "Ubuntu", npm = "1234567890", alamat = "Fasilkom UI")
        response = Client().post('/lab-7/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())

    def test_friend_description_can_be_access(self):
        friend = Friend.objects.create(friend_name = "Ubuntu", npm = "1234567890", alamat = "Fasilkom UI")
        response = Client().post('/lab-7/friend-description/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})

    def test_model_to_dict(self):
        data = Friend.objects.create(friend_name='Hoho', npm='0987654321', alamat="Fasilkom UI")
        dataNow = model_to_dict(data)
        self.assertEqual(type(dataNow), str)

    def test_invalid_sso_raise_exception(self):
        username_to_input = "hehe"
        password_to_input = "hehe123"
        csui_helper = CSUIhelper()
        csui_helper.instance.username = username_to_input
        csui_helper.instance.password = password_to_input
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token()
        self.assertIn(username_to_input, str(context.exception))

    def test_get_client_id(self):
        csui_helper = CSUIhelper()
        client_id = csui_helper.instance.get_client_id()
        self.assertEqual(client_id, "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")
    
    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])