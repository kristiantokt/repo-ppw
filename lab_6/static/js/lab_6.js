function chat(e) {
    var text = "";
    var key = e.keyCode;
    if (key === 13) {
        var message = $("#text").val();
        document.getElementById("message").innerHTML += ('<div class="msg-send">' + message + '</div>');
        document.getElementById("text").value = "";
    }
}

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
    if (x === 'ac') {
        print.value = ''
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        print.value = evil(print.value);
        erase = true;
    } else {
        print.value += x;
    }
};

function evil(fn) {
    return new Function('return ' + fn)();
}

$(document).ready(function() {
    themes = JSON.stringify([
        { "id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA" },
        { "id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA" },
        { "id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA" },
        { "id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA" },
        { "id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121" },
        { "id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121" },
        { "id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121" },
        { "id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121" },
        { "id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121" },
        { "id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121" },
        { "id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA" }
    ]);

    localStorage.setItem("themes", themes);

    $('.my-select').select2({
        'data': JSON.parse(localStorage.getItem('themes'))
    });
    changeTheme(3);
    $('.apply-button').on('click', function() {
        var valOfMySelect = $('.my-select').select2().val();
        changeTheme(valOfMySelect);
    })
});

function changeTheme(x) {
    var indexSelectedTheme = x;
    var themeArray = JSON.parse(localStorage.getItem("themes"));
    var selectedTheme = themeArray[x];
    $('body').css({
        "background-color": selectedTheme.bcgColor,
        "color": selectedTheme.fontColor
    })
}