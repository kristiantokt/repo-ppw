# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-27 06:54
from __future__ import unicode_literals

from django.db import migrations, models
import lab_4.models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0003_auto_20170924_2007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=lab_4.models.Message.convertTimezone),
        ),
    ]
