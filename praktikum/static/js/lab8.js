window.fbAsyncInit = () => {
    FB.init({
        appId: '160478411362757',
        cookie: true,
        xfbml: true,
        version: 'v2.11'
    });

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            render(true);
        } else if (response.status === 'not_authorized') {
            render(false);
        } else {
            render(false);
        }
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

const render = loginFlag => {
    if (loginFlag) {
        getUserData(user => {
            $('#lab8').html(
                '<div class="profile col-md-10 col-md-offset-1">' +
                '<img class="cover img-rounded img-responsive" src="' + user.cover.source + '" alt="cover"/>' +
                '<img class="picture img-circle img-responsive" src="' + user.picture.data.url + '" alt="profpic" />' +
                '<div class="data">' +
                '<h1 id="username">' + user.name + '</h1>' +
                '<h3 id="useremail">' + user.email + ' - ' + user.gender + '</h3>' +
                '</div>' +
                '</div>' +
                '<div class="controller col-md-10 col-md-offset-1">' +
                '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
                '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
                '<button class="logout" onclick="facebookLogout()">Logout</button>' +
                '</div>' +
                '<div class="feeds col-md-10 col-md-offset-1">' +
                '<hr>' +
                '<h2 id="feedTitle">' + 'Current Feeds' + '</h2>' +
                '</div>'
            );

            getUserFeed(feed => {
                feed.data.map(value => {
                    options = {
                        day: 'numeric',
                        month: 'numeric',
                        year: 'numeric',
                        hour: 'numeric',
                        minute: 'numeric',
                        hour12: false,
                        timeZone: 'Asia/Jakarta'
                    };
                    var time = new Intl.DateTimeFormat('id-ID', options).format(new Date(value.created_time));
                    if (value.message && value.story) {
                        $('.feeds').append(
                            '<div class="feed">' +
                            '<h1>' + value.message + '</h1>' +
                            '<h2>' + value.story + '</h2>' +
                            '<p>Posted on ' + time + '</p>' +
                            '</div>'
                        );
                    } else if (value.message) {
                        $('.feeds').append(
                            '<div class="feed">' +
                            '<h1>' + value.message + '</h1>' +
                            '<p>Posted on ' + time + '</p>' +
                            '</div>'
                        );
                    } else if (value.story) {
                        $('.feeds').append(
                            '<div class="feed">' +
                            '<h2>' + value.story + '</h2>' +
                            '<p>Posted on ' + time + '</p>' +
                            '</div>'
                        );
                    }
                });
            });
        });
    } else {
        // Tampilan ketika belum login
        $('#lab8').html('<h1 id="opening">Welcome to my Mini-Facebook</h1>');
        $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
};

const facebookLogin = () => {
    FB.login(function(response) {
        console.log(response);
        render(true);
    }, { scope: 'public_profile,user_posts,publish_actions,user_about_me' })
};

const facebookLogout = () => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout();
            render(false);
        }
    });

};

const getUserData = (fun) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.api('/me?fields=id,name,about,email,gender,cover,picture', 'GET', function(response) {
                console.log(response);
                if (response && !response.error) {
                    fun(response);
                } else {
                    alert("Something went wrong");
                }
            });
        }
    });
};

const getUserFeed = (fun) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.api('/me/posts', 'GET', function(response) {
                console.log(response);
                if (response && !response.error) {
                    fun(response);
                } else {
                    alert("Something went wrong");
                }
            });
        }
    });

};

const postFeed = (message) => {
    FB.api('/me/feed', 'POST', { message: message });
    render(true);
};

const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
};