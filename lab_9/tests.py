from django.test import TestCase
from django.test import Client
import lab_9.csui_helper as csui_helper
import lab_9.api_enterkomputer as api_enterkomputer
import lab_9.custom_auth as custom_auth
import environ

from django.test.utils import override_settings

root = environ.Path('__file__')
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env()
# Create your tests here.

class Lab9UnitTest(TestCase):
    """
    UnitTest for views.py
    """
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_page_when_user_is_logged_in_or_not(self):
        # not logged in, render login template
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_9/session/login.html')

        # logged in, redirect to profile page
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')

    def test_direct_access_to_profile_url(self):
        # not logged in, redirect to login page
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

        # logged in, render profile template
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_add_delete_and_reset_favorite_drones(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)

        # add drone
        response = self.client.post('/lab-9/add_session_drones/' + api_enterkomputer.get_drones().json()[0]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah drone favorite", html_response)

        response = self.client.post('/lab-9/add_session_drones/' + api_enterkomputer.get_drones().json()[1]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertIn("Berhasil tambah drone favorite", html_response)

        # delete drone
        response = self.client.post('/lab-9/del_session_drones/' + api_enterkomputer.get_drones().json()[0]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus dari favorite", html_response)

        # reset drones
        response = self.client.post('/lab-9/clear_session_drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite drones", html_response)

    def test_add_delete_and_reset_favorite_item(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-9/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)

        # add soundcard
        response = self.client.post('/lab-9/add_session_item/soundcards/' + api_enterkomputer.get_soundcards().json()[0]["id"] + '/')
        response = self.client.post('/lab-9/add_session_item/soundcards/' + api_enterkomputer.get_soundcards().json()[1]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        # delete soundcard
        response = self.client.post('/lab-9/del_session_item/soundcards/' + api_enterkomputer.get_soundcards().json()[0]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        # reset soundcards
        response = self.client.post('/lab-9/clear_session_item/soundcards/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        # add optical
        response = self.client.post('/lab-9/add_session_item/opticals/' + api_enterkomputer.get_opticals().json()[0]["id"] + '/')
        response = self.client.post('/lab-9/add_session_item/opticals/' + api_enterkomputer.get_opticals().json()[1]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        # delete optical
        response = self.client.post('/lab-9/del_session_item/opticals/' + api_enterkomputer.get_opticals().json()[0]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        # reset opticals
        response = self.client.post('/lab-9/clear_session_item/opticals/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

    """
    UnitTest for csui_helper.py
    """
    def test_get_access_token(self):
        username_to_input="hehehe"
        password_to_input="123456"
        with self.assertRaises(Exception) as context:
            csui_helper.get_access_token(username_to_input, password_to_input)
        self.assertIn(username_to_input, str(context.exception))

    def test_get_client_id(self):
        self.assertEqual(csui_helper.get_client_id(), 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

    def test_verify_user(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        access_token = csui_helper.get_access_token(username, password)
        verify_user = csui_helper.verify_user(access_token)
        self.assertTrue(verify_user is not None)

    def test_get_data_user(self):
        access_token = csui_helper.get_access_token(env("SSO_USERNAME"), env("SSO_PASSWORD"))
        verify_user = csui_helper.verify_user(access_token)
        data_user = csui_helper.get_data_user(access_token, verify_user["identity_number"])

    def test_get_drones(self):
        data = api_enterkomputer.get_drones()
        self.assertTrue(data is not None)

    def test_get_soundcard(self):
        data = api_enterkomputer.get_soundcards()
        self.assertTrue(data is not None)

    def test_get_optical(self):
        data = api_enterkomputer.get_opticals()
        self.assertTrue(data is not None)

    """
    UnitTest for custom_auth.py
    """
    def test_for_login_and_logout(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = Client().post('/lab-9/custom_auth/login/', {'username': username, 'password':password})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')
        response = Client().get('/lab-9/custom_auth/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/login.html')


    """
    UnitTest for cookies
    """

    def test_cookie(self):
        # not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

        # login using HTTP GET method
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        # login failed, invalid pass and uname
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'username', 'password': 'password'})
        html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Username atau Password Salah", html_response)

        # try to set manual cookies
        self.client.cookies.load({"user_login": "u", "user_password": "p"})
        response = self.client.get('/lab-9/cookie/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Kamu tidak punya akses :P ", html_response)

        # login successed
        self.client = Client()
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'kristiantokt', 'password': '28071998'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

        # logout
        response = self.client.post('/lab-9/cookie/clear/')
        html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Cookies direset", html_response)